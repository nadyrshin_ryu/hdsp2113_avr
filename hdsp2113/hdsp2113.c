//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include <inavr.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <delay.h>
#include <f6x8m.h>
#include <hdsp2113.h>


// ������� ��� ���������� ����������� ��� ������ �������� ����� GPIO ���������������� 
#if HDSP_Reset_PinUsed
#define HDSP_RESET_HIGH()       HDSP_Reset_Port |= HDSP_Reset_Mask
#define HDSP_RESET_LOW()        HDSP_Reset_Port &= ~HDSP_Reset_Mask
#endif
#if HDSP_FL_PinUsed
#define HDSP_FL_HIGH()          HDSP_FL_Port |= HDSP_FL_Mask
#define HDSP_FL_LOW()           HDSP_FL_Port &= ~HDSP_FL_Mask
#endif
#if HDSP_RD_PinUsed
#define HDSP_RD_HIGH()          HDSP_RD_Port |= HDSP_RD_Mask
#define HDSP_RD_LOW()           HDSP_RD_Port &= ~HDSP_RD_Mask
#endif
#if HDSP_WR_PinUsed
#define HDSP_WR_HIGH()          HDSP_WR_Port |= HDSP_WR_Mask
#define HDSP_WR_LOW()           HDSP_WR_Port &= ~HDSP_WR_Mask
#endif
#define HDSP_CE_HIGH()          HDSP_CE_Port |= HDSP_CE_Mask
#define HDSP_CE_LOW()           HDSP_CE_Port &= ~HDSP_CE_Mask
#define HDSP_ADDR_SET(val)      {HDSP_Addr_Port &= ~(0x1F << HDSP_Addr_Shift); HDSP_Addr_Port |= (val & 0x1F) << HDSP_Addr_Shift;}
#define HDSP_DATA_SET(val)      HDSP_Data_Port = val
#if HDSP_RD_PinUsed
#define HDSP_DATA_GET()         HDSP_Data_Pin
#endif
#define HDSP_DATA_IN()          HDSP_Data_DDR = 0x00
#define HDSP_DATA_OUT()         HDSP_Data_DDR = 0xFF


static uint8_t ControlReg = 0;
char hdsp_StrBuff[HDSP_COLS];          // ����� ������ ��� ������


#if HDSP_RD_PinUsed
//==============================================================================
// ������� ������ ���� �� ����������� ����������
//==============================================================================
uint8_t hdsp_ReadByte(uint8_t Addr, uint8_t Flash)
{
  HDSP_ADDR_SET(Addr);

#if HDSP_FL_PinUsed
  if (Flash)
    HDSP_FL_LOW();
  else
    HDSP_FL_HIGH();
#endif
  
  HDSP_CE_LOW();
  
  HDSP_RD_LOW();
  HDSP_RD_HIGH();
  
  HDSP_DATA_IN();
  uint8_t Data = HDSP_DATA_GET();
  HDSP_DATA_OUT();
  
  HDSP_CE_HIGH();
  
  return Data;
}
//==============================================================================
#endif

//==============================================================================
// ������� ����� ���� � ���������� ����������
//==============================================================================
void hdsp_WriteByte(uint8_t Addr, uint8_t Flash, uint8_t Data)
{
  HDSP_ADDR_SET(Addr);

#if HDSP_FL_PinUsed
  if (Flash)
    HDSP_FL_LOW();
  else
    HDSP_FL_HIGH();
#endif
  
  HDSP_CE_LOW();
  
#if HDSP_WR_PinUsed
  HDSP_WR_LOW();
#endif
  HDSP_DATA_SET(Data);
#if HDSP_WR_PinUsed
  HDSP_WR_HIGH();
#endif
  
  HDSP_CE_HIGH();
}
//==============================================================================

#if HDSP_RD_PinUsed
//==============================================================================
// ������� ��������� �������� ������������ �������� ����������� ����������
//==============================================================================
uint8_t hdsp_ControlReg_Read(void)
{
  ControlReg = hdsp_ReadByte(0x10, 1);
  return ControlReg;
}
//==============================================================================
#endif

//==============================================================================
// ������� ���������� ���� � ����������� ������� ����������� ����������
//==============================================================================
void hdsp_ControlReg_Write(void)
{
  hdsp_WriteByte(0x10, 1, ControlReg);
}
//==============================================================================


//==============================================================================
// ��������� ���������� ����� ����������� ���������� � �������� ������ �����
//==============================================================================
void hdsp_Reset(void)
{
  HDSP_CE_HIGH();
  HDSP_RESET_LOW();
  delay_us(1);
  HDSP_RESET_HIGH();
  delay_us(120);        // ����� ��������� ����� 110 ���
}
//==============================================================================


//==============================================================================
// ��������� ������� ���������
//==============================================================================
void hdsp_Clear(void)
{
  // ������� ������ ������
  memset(hdsp_StrBuff, ' ', HDSP_COLS);

  ControlReg |= (1 << 7);
  hdsp_ControlReg_Write();

#if HDSP_RD_PinUsed
  uint16_t TimeOutCntr = 30000;
  while ((TimeOutCntr) && (!(hdsp_ControlReg_Read() & (1 << 7))))  // ������� ��������� ����� ����������
    TimeOutCntr--;
#else
  delay_us(120);        // ����� ��������� �� ����� 110 ���
#endif
}
//==============================================================================


//==============================================================================
// ��������� ��������� ���� ����������
//==============================================================================
void hdsp_SelfTest(void)
{
  ControlReg |= (1 << 6);
  hdsp_ControlReg_Write();

#if HDSP_RD_PinUsed
  uint16_t TimeOutCntr = 30000;
  while ((TimeOutCntr) && (!(hdsp_ControlReg_Read() & (1 << 5))))  // ������� ��������� ����� ����������
    TimeOutCntr--;
#else
  delay_ms(5000);
#endif
}
//==============================================================================


#if HDSP_FL_PinUsed
//==============================================================================
// ��������� ��������/��������� ������� ���������� ���������
//==============================================================================
void hdsp_Flash(uint8_t OnOff)
{
  if (OnOff)
    ControlReg |= (1 << 3);
  else
    ControlReg &= ~(1 << 3);
  
  hdsp_ControlReg_Write();
}
//==============================================================================
#endif


//==============================================================================
// ��������� ��������/��������� ������� ���� ���������
// Blink �������� ������� Flash
//==============================================================================
void hdsp_Blink(uint8_t OnOff)
{
  if (OnOff)
    ControlReg |= (1 << 4);
  else
    ControlReg &= ~(1 << 4);
  
  hdsp_ControlReg_Write();
  delay_us(120);        // ����� ��������� �� ����� 110 ���
}
//==============================================================================


//==============================================================================
// ��������� ������������� ������� ���������� (0-7)
//==============================================================================
void hdsp_SetBrightness(uint8_t Value)
{
  if (Value > 7)
    Value = 7;
  
  ControlReg &= 0xF8;
  ControlReg |= (7 - Value);

  hdsp_ControlReg_Write();
}
//==============================================================================


#if HDSP_RD_PinUsed
//==============================================================================
// ��������� ������ ������ (7 ����) �� UDC RAM
// � ������ ������� �������� - ������� 5 ���
//==============================================================================
void hdsp_CustomChar_Read(uint8_t CharNum, uint8_t *CharBuff)
{
  // ������������� ����� � UDC RAM
  hdsp_WriteByte(0, 1, CharNum);
  
  // ����� ����� ���������� �������
  for (uint8_t i = 0; i < 7; i++)
    *(CharBuff++) = hdsp_ReadByte((1 << 3) | (i & 0x7), 1);
}
//==============================================================================
#endif


//==============================================================================
// ��������� ����� ������ (7 ����) � UDC RAM
// � ������ ������� �������� - ������� 5 ���
//==============================================================================
void hdsp_CustomChar_Write(uint8_t CharNum, uint8_t *CharBuff)
{
  // ������������� ����� � UDC RAM
  hdsp_WriteByte(0, 1, CharNum);
  
  // ����� ����� ���������� �������
  for (uint8_t i = 0; i < 7; i++)
    hdsp_WriteByte((1 << 3) | (i & 0x7), 1, *(CharBuff++));
}
//==============================================================================


#if HDSP_FL_PinUsed
//==============================================================================
// ��������� ��������/��������� ������� ������� �� �������
//==============================================================================
void hdsp_FlashChar_OnOff(uint8_t CharNum, uint8_t OnOff)
{
  if (CharNum > HDSP_COLS)
    return;
  
  if (OnOff)
    OnOff = 1;
  
  hdsp_WriteByte(CharNum, 0, OnOff);
}
//==============================================================================
#endif


//==============================================================================
// ��������� ������������� ��� ����������
//==============================================================================
void hdsp_bus_init(void)
{
  // �������������� ����
#if HDSP_Reset_PinUsed
  HDSP_Reset_DDR |= HDSP_Reset_Mask;
  HDSP_RESET_LOW();
#endif
#if HDSP_RD_PinUsed
  HDSP_RD_DDR |= HDSP_RD_Mask;
  HDSP_RD_HIGH();
#endif
#if HDSP_FL_PinUsed
  HDSP_FL_DDR |= HDSP_FL_Mask;
  HDSP_FL_HIGH();
#endif
#if HDSP_WR_PinUsed
  HDSP_WR_DDR |= HDSP_WR_Mask;
  HDSP_WR_HIGH();
#endif

  HDSP_CE_DDR |= HDSP_CE_Mask;
  HDSP_CE_HIGH();
  HDSP_Addr_DDR |= (0x1F << HDSP_Addr_Shift);
  HDSP_ADDR_SET(0x00);
  HDSP_Data_DDR = 0xFF;
  HDSP_DATA_SET(0x00);
}
//==============================================================================


//==============================================================================
// ��������� ������������� ������� (�������� ������������������ ������ � �������)
//==============================================================================
void hdsp_start(void)
{
  // ����� ����������
  hdsp_Reset();
  // ������� ������ ����������
  hdsp_Clear();
}
//==============================================================================


//==============================================================================
// ��������� ������������� �������
//==============================================================================
void hdsp_init(void)
{
  hdsp_bus_init();
  hdsp_start();
}
//==============================================================================


//==============================================================================
// ��������� ������� �� ��������� ���������� ������ ����� (hdsp_StrBuff)
//==============================================================================
void hdsp_UpdateFromBuff(void)
{
  uint8_t UserCharNum = 0;
  uint8_t CharBuff[7];
  
  // ���������� ������ ��������, ������� ����� ��������� � UDC-������ ����������
  // ��������� � UDC-������ ���������� ����������� �������
  for (uint8_t i = 0; i < HDSP_COLS; i++)
  {
    if (hdsp_StrBuff[i] > 127)  // ����� ��������� ������ � UDC
    {
      uint8_t *ch = f6x8m_GetCharTable(hdsp_StrBuff[i]);
      ch += 2;
      
      // �������������� ������� �������� ������ �������
      for (uint8_t Row = 0; Row < 7; Row++)
        CharBuff[Row] = *(ch++) >> 3;
      
      // ��������� ����� ������� � UDC RAM
      hdsp_CustomChar_Write(UserCharNum, CharBuff);
      // ����� � Character RAM ��� ������������ ������ ������� 
      hdsp_WriteByte(0x18 | i, 1, UserCharNum | 0x80);

      UserCharNum++;
    }
    else        // ������ ��������� �� �����
      hdsp_WriteByte(0x18 | i, 1, hdsp_StrBuff[i]);
  }
}
//==============================================================================


//==============================================================================
// ��������� �������� ANSI-������ � �������� ������� ����������
// StartIdx - ��������� ���������� ��� ������
//==============================================================================
void hdsp_puts(uint8_t StartIdx, char *str)
{
  while (*str != '\0')
  {
    hdsp_StrBuff[StartIdx++] = *(str++);
    
    // ����� �� ����� ����������
    if (StartIdx >= HDSP_COLS)
      break;
  }
  
  hdsp_UpdateFromBuff();
}
//==============================================================================



//==============================================================================
// ��������� ���������������� ������ ������� � �������� �������
//==============================================================================
void hdsp_printf(uint8_t StartIdx, const char *args, ...)
{
  // ������ ��������� ����� � ����
  char TempBuff[9];
  
  va_list ap;
  va_start(ap, args);
  char len = vsnprintf(TempBuff, sizeof(TempBuff), args, ap);
  va_end(ap);

  hdsp_puts(StartIdx, TempBuff);
}
//==============================================================================
