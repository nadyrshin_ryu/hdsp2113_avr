//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _HDSP2113_H
#define _HDSP2113_H

#include <stdint.h>


#define HDSP_COLS               8       // ���-�� ��������� ����������

// ���� ����, ��� ������������ ���������� ����������������� ������ RESET ����������
#define HDSP_Reset_PinUsed      1       // ���� ���� ����, �� ����� ����������� ���������� �� ��������.
                                        // ����� Reset ����� ��������� � ���. 1
// ���� ����, ��� ������������ ���������� ����������������� ������ RD ����������
#define HDSP_RD_PinUsed         0       // ���� ���� ����, �� ������ �� ����������� ���������� �� ��������. 
                                        // ����� RD ����� ��������� � ���. 1
// ���� ����, ��� ������������ ���������� ����������������� ������ WR ����������
#define HDSP_WR_PinUsed         0       // ���� ���� ����, �� ���������� ������� �� ������. 
                                        // ����� WR � ���� ������ ����� ��������� � ���. 0.
// ���� ����, ��� ������������ ���������� ����������������� ������ FL ����������
#define HDSP_FL_PinUsed         0       // ���� ���� ����, �� ������� ������� �������� �� ��������. 
                                        // ����� FL ����� ��������� � ���. 1


//// ���� ����������������, ����������� ����������� hdsp2113
// ������ RESET
#if HDSP_Reset_PinUsed
#define HDSP_Reset_Port         PORTC
#define HDSP_Reset_DDR          DDRC
#define HDSP_Reset_Mask         (1 << 4)
#endif
// ������ FL (FLASH)
#if HDSP_FL_PinUsed
#define HDSP_FL_Port            PORTB
#define HDSP_FL_DDR             DDRB
#define HDSP_FL_Mask            (1 << 7)
#endif
// ������ RD (READ)
#if HDSP_RD_PinUsed
#define HDSP_RD_Port            PORTB
#define HDSP_RD_DDR             DDRB
#define HDSP_RD_Mask            (1 << 7)
#endif
// ������ WR (WRITE)
#if HDSP_WR_PinUsed
#define HDSP_WR_Port            PORTB
#define HDSP_WR_DDR             DDRB
#define HDSP_WR_Mask            (1 << 7)
#endif
// ������ CE (CHIP ENABLE)
#define HDSP_CE_Port            PORTC
#define HDSP_CE_DDR             DDRC
#define HDSP_CE_Mask            (1 << 5)
// ���� ���� ������ (���� A0-A4) ������������� ���������� HDSP
#define HDSP_Addr_Port          PORTB
#define HDSP_Addr_DDR           DDRB
#define HDSP_Addr_Shift         0      // ����� ��� ������ � ����� GPIO (���� 0 - ������������ ������� 5 ��� �����)
// ���� ���� ������ (���� D0-D7) ������������� ���������� HDSP
#define HDSP_Data_Port          PORTD
#define HDSP_Data_DDR           DDRD
#define HDSP_Data_Pin           PIND



// ��������� ������������� �������
void hdsp_init(void);
// ��������� ���������� ����� ����������� ���������� � �������� ������ �����
void hdsp_Reset(void);
// ��������� ������� ���������
void hdsp_Clear(void);
// ��������� ��������� ���� ����������
void hdsp_SelfTest(void);
// ��������� ��������/��������� ������� ���� ���������
void hdsp_Blink(uint8_t OnOff);
// ��������� ������������� ������� ���������� (0-7)
void hdsp_SetBrightness(uint8_t Value);
// ��������� �������� ANSI-������ � �������� ������� ����������
// StartIdx - ��������� ���������� ��� ������
void hdsp_puts(uint8_t StartIdx, char *str);
// ��������� ���������������� ������ ������� � �������� �������
void hdsp_printf(uint8_t StartIdx, const char *args, ...);

#if HDSP_FL_PinUsed
// ��������� ��������/��������� ������� ������� �� �������
void hdsp_FlashChar_OnOff(uint8_t CharNum, uint8_t OnOff);
#endif

#endif